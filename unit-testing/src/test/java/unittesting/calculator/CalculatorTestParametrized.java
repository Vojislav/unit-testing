package unittesting.calculator;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTestParametrized {
	
	private Calculator calculatorObject = new Calculator();
	
	@BeforeTest
	public void metodPreTestCase(){
		calculatorObject.clear();
	}
	
	@Test()
	@Parameters({"zaSabiranje"})
	public void testAdd(int zaSabiranje){
		int vrednostKalkulatoraPreSabiranja = calculatorObject.getResult();
		// dodam novu vrednost na tu postojecu
		calculatorObject.add(zaSabiranje);
		int ocekivaniZbir = vrednostKalkulatoraPreSabiranja + zaSabiranje;
		int stvarniZbir = calculatorObject.getResult();
		assertEquals(ocekivaniZbir, stvarniZbir);
	}
	
	@Test()
	@Parameters({"zaOduzimanje"})
	public void testSub(int zaOduzimanje){
		int vrednostKalkulatoraPreOduzimanja = calculatorObject.getResult();
		// oduzmem novu vrednost od postojece u kalkulatoru
		calculatorObject.substract(zaOduzimanje);
		int ocekivanaRazlika = vrednostKalkulatoraPreOduzimanja - zaOduzimanje;
		int stvarnaRazlika = calculatorObject.getResult();
		assertEquals(ocekivanaRazlika, stvarnaRazlika);
	}
	
	@Test()
	@Parameters({"zaMnozenje"})
	public void testMul(int zaMnozenje){
		int vrednostKalkulatoraPreMnozenja = calculatorObject.getResult();
		// pomnozim novu vrednost sa postojecom u kalkulatoru
		calculatorObject.multiply(zaMnozenje);
		int ocekivaniProizvod = vrednostKalkulatoraPreMnozenja * zaMnozenje;
		int stvarniProizvod = calculatorObject.getResult();
		assertEquals(ocekivaniProizvod, stvarniProizvod);
	}
	
	@Test()
	@Parameters({"zaDeljenje"})
	public void testDiv(int zaDeljenje){
		int vrednostKalkulatoraPreDeljenja = calculatorObject.getResult();
		// podelim vrednost u kalkulatoru sa novom vrednoscu
		calculatorObject.divide(zaDeljenje);
		int ocekivaniKolicnik = vrednostKalkulatoraPreDeljenja / zaDeljenje;
		int stvarniKolicnik = calculatorObject.getResult();
		assertEquals(ocekivaniKolicnik, stvarniKolicnik);
	}
	
	@Test()
	@Parameters({"zaKvadriranje"})
	public void testSquare(int zaKvadriranje){
		// kvadriram broj putem kalkulatora
		calculatorObject.square(zaKvadriranje);
		int ocekivaniRezultat = zaKvadriranje * zaKvadriranje;
		int stvarniRezultat = calculatorObject.getResult();
		assertEquals(ocekivaniRezultat, stvarniRezultat);
	}
}
