package unittesting.calculator;

import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTestSquare {
	
	// OZANAKA GRUPE JE DEO ZADATKA 5, bez toga bi bilo samo @Test
	@Test(groups = {"odabranaMetoda"})
	public void testSquare(){
		Calculator calculatorObject = new Calculator();
		// metoda za kvadriranje prima broj n, kvadrira ga i stavi ga u rezultat kalkulatora
		// sada kvadriram recimo broj 3
		calculatorObject.square(3);
		int kvadriranBroj3 = calculatorObject.getResult();
		int ocekivaniRezultat = 3*3;
		assertEquals(ocekivaniRezultat, kvadriranBroj3);
	}
	
	@Test(timeOut=1000)
	public void testSquareRoot(){
		Calculator calculatorObject = new Calculator();
		// metoda za korenovanje prima broj n, korenuje ga i stavi ga u rezultat kalkulatora
		// sada korenujem recimo broj 16
		calculatorObject.squareRoot(16);
		int korenBroja16 = calculatorObject.getResult();
		int ocekivaniRezultat = 4;
		assertEquals(ocekivaniRezultat, korenBroja16);
		
		// metoda ima beskonacnu petlju i nikada se ne zavrsava
		// timeout ce to proveriti tako sto ce vremenski ograniciti
		// izvrsavanje i test ce pasti
	}
}
