package unittesting.calculator;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class CalculatorTestAddSub {
	
	private Calculator calculatorObject = new Calculator();
	
	@BeforeMethod
	public void methodThatRunsBeforeEveryOtherMethodInThisClass() {
		calculatorObject.clear();
	}
	
	// OZANAKA GRUPE JE DEO ZADATKA 5, bez toga bi bilo samo @Test
	@Test(groups = {"odabranaMetoda"})
	public void testAdd(){
		// pre ove metode se pokrenula metoda anotirana sa @BeforeMethod i setovala je
		// vrednost kalkulatora na 0. Sada na tu vrednost saberemo jos broj 5 i proverimo
		// da li je u kalkulatoru sada vrednost 5
		calculatorObject.add(5);
		int novaVrednost = calculatorObject.getResult();
		assertEquals(5, novaVrednost);
	}
	
	@Test
	public void testSub(){
		// pre ove metode se pokrenula metoda anotirana sa @BeforeMethod i setovala je
		// vrednost kalkulatora na 0. Sada od te vrednosti oduzmemo broj 5 i proverimo
		// da li je u kalkulatoru sada vrednost -5. Test treba da padne posto je u metodi
		// substract() namerno napravljena greska. Eto, testom smo nasli gresku, sto i 
		// jeste njegov zadatak
		calculatorObject.substract(5);
		int novaVrednost = calculatorObject.getResult();
		assertFalse(!(novaVrednost==-5));
	}
	
	
	
	
	
	
	/*
	 * DEO ZADATKA 5
	 * U od klasa moramo staviti metodu koja se pokrece pre izvrsavanja date grupe
	 */
	@BeforeGroups(groups={"odabranaMetoda"})
	public void preGrupeOdabranihMetoda(){
		System.out.println("Sada će se pokrenuti svi testovi iz grupe ODABRANIH METODA.");
	}
	
	@AfterGroups(groups={"odabranaMetoda"})
	public void posleGrupeOdabranihMetoda(){
		System.out.println("Svi testovi iz grupe ODABRANIH METODA su izvršeni.");
	}
}
