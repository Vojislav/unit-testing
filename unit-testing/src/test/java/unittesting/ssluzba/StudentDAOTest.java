package unittesting.ssluzba;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import unittesting.book.BookDAO;
import unittesting.ssluzba.model.Student;
import unittesting.ssluzba.service.StudentService;

public class StudentDAOTest {
	
	private static StudentService mockedStudent;
	private static Student student1;
	private static Student student2;
	private static Student student3;
	private static ArrayList<Student> studenti;
	
	@BeforeClass
	public static void setUp(){
		mockedStudent = mock(StudentService.class);
		student1 = new Student((long) 0, "000", "Ime1", "Prezime1", null, null);
		student2 = new Student((long) 1, "001", "Ime2", "Prezime2", null, null);
		student3 = new Student((long) 2, "002", "Ime3", "Prezime3", null, null);
		ArrayList<Student> studenti = new ArrayList<Student>();
		studenti.add(student1);
		studenti.add(student2);
		studenti.add(student3);
		when(mockedStudent.findOne((long) 0)).thenReturn(student1);
		when(mockedStudent.findAll()).thenReturn(studenti);
		when(mockedStudent.findByCard("001")).thenReturn(student2);
		when(mockedStudent.findByLastName("Prezime3")).thenReturn((List<Student>) student3);
		when(mockedStudent.save(student1)).thenReturn(0);
	}
	
	@Test
	public void testFindOne(){
		long id = 0;
		Assert.assertEquals(student1,mockedStudent.findOne(id));
	}
	
	@Test
	public void testFindAll(){
		Assert.assertEquals(studenti,mockedStudent.findAll());
	}
	
	@Test
	public void testFindByCard(){
		Assert.assertEquals(student2,mockedStudent.findByCard("001"));
	}
	
	@Test public void testFindByLastName(){
		Assert.assertEquals((List<Student>)student3,mockedStudent.findByLastName("Prezime3"));
	}
	
	@Test
	public void testSave(){
		Assert.assertEquals(0,mockedStudent.save(student1));
	}
}
