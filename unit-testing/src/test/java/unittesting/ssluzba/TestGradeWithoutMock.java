package unittesting.ssluzba;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import unittesting.ssluzba.service.StudentService;

public class TestGradeWithoutMock {

	StudentService sluzba = new StudentService();
	
	@DataProvider (name = "provajder")
	public static Object[][] provideData() {

		return new Object[][] { 
			{ 55,6 }, 
			{ 68,7 }, 
			{ 80,8 },
			{ 92,9 },
			{ 99,10}
		};
	}
	
	@Test(dataProvider = "provajder")
	public void testOcena(int poeni,int ocena){
		Assert.assertEquals(ocena,sluzba.calculateGrade(poeni));
	}
}
