package unittesting.ssluzba;

import static org.mockito.Mockito.when;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import unittesting.ssluzba.service.StudentService;

public class TestGradeMock {

	private static StudentService mockedSluzba;
	
	@BeforeClass
	private static void setUp(){
	StudentService mockedSluzba = Mockito.mock(StudentService.class);
	when(mockedSluzba.calculateGrade(55)).thenReturn(6);
	when(mockedSluzba.calculateGrade(68)).thenReturn(7);
	when(mockedSluzba.calculateGrade(80)).thenReturn(8);
	when(mockedSluzba.calculateGrade(92)).thenReturn(9);
	when(mockedSluzba.calculateGrade(99)).thenReturn(10);
	}
	
	@Test(dataProvider = "provajder",dataProviderClass=unittesting.ssluzba.TestGradeWithoutMock.class)
	public void testOcenaMock (int poeni,int ocena){
		Assert.assertEquals(ocena,mockedSluzba.calculateGrade(poeni));
	}
}
