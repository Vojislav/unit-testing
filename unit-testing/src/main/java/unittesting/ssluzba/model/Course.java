package unittesting.ssluzba.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


public class Course {

	private Long id;
	private String name;
	
	private Set<Enrollment> enrollments = new HashSet<Enrollment>();
	private Set<Exam> exams = new HashSet<Exam>();
	private Set<Teacher> teachers = new HashSet<Teacher>();
	
	
	public Course(Long id, String name, Set<Enrollment> enrollments, Set<Exam> exams, Set<Teacher> teachers) {
		super();
		this.id = id;
		this.name = name;
		this.enrollments = enrollments;
		this.exams = exams;
		this.teachers = teachers;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Enrollment> getEnrollments() {
		return enrollments;
	}

	public void setEnrollments(Set<Enrollment> enrollments) {
		this.enrollments = enrollments;
	}
	
	public Set<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(Set<Teacher> teachers) {
		this.teachers = teachers;
	}
	
	public Set<Exam> getExams() {
		return exams;
	}

	public void setExams(Set<Exam> exams) {
		this.exams = exams;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Course c = (Course) o;
        if(c.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, c.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + "]";
	}
}
